package main;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import cello.tablet.*;

public class Smain extends PApplet {
	private static final long serialVersionUID = 1L;
	static final int WIDTH = 512;
	static final int HEIGHT = 512;
	File dir;
	File [] images;
	int n = 0;
	
	//current info
	PImage currentImage;
	StrokeField currentField;
	
	PVector pMouse;
	public static void main(String args[]) 
	{
		PApplet.main(new String[] {"main.Smain"} );
	}
	public void setup()
	{
		size(WIDTH,HEIGHT);
		background(0);
		/*
		try 
		{
			JTablet jtablet = new JTablet();
		} catch (JTabletException jte) 
		{
		    System.err.println("Could not load JTablet! (" + jte.toString() + ").");
		}*/
		
		dir = new File("data");
		images = dir.listFiles(new ImageFileFilter());
		println(images);
		loadCurrentImage();
	}
	public void loadCurrentImage()
	{
		if(images.length > n)
		{
			currentImage = loadImage("data/" + images[n].getName());
			currentField = new StrokeField(currentImage.width/5,currentImage.width/5);
		}
	}
	//asumes 3 character extension, e.g. .jpeg no good
	public String currentImageName()
	{
		return images[n].getName().substring(0, images[n].getName().length() - 4);
	}
	public void draw()
	{
		if(n < images.length)
		{
			//TODO draw the nth image
			int iw = currentImage.width;
			int ih = currentImage.height;
			int dw = WIDTH; int dh = HEIGHT;
			pushMatrix();
			rotate(-PI/2);
			translate(-dh,0);
			image(currentImage,0,0,dh,dw);
			popMatrix();
			currentField.draw(this);
		}
		else
		{
			exit();
		}
	}
	public void keyPressed()
	{
		if(keyCode == ESC)
			exit();
		if (keyCode == ENTER)
		{
			//currentField = new StrokeField(currentImage.width/10,currentImage.height/10);
			currentField = new StrokeField(30,30);
		}
		if (keyCode == SHIFT)
		{
			try {
				currentField.writeFile(new FileOutputStream(currentImageName() + ".dat"));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			n+=1;
			loadCurrentImage();
		}
	}
	public void mousePressed()
	{
		pMouse = new PVector(mouseX,mouseY);
	}
	public void mouseReleased()
	{
	}
	//this code is shot, mouse moved is not called when mouse button is down
	public void mouseDragged()
	{
		currentField.stroke(this, new FVector(pMouse.x,pMouse.y),new FVector(mouseX,mouseY));
		pMouse = new PVector(mouseX,mouseY);
	}
}
