package main;

import java.util.LinkedList;
import java.util.List;
import java.io.*;

import com.sun.xml.internal.bind.marshaller.DataWriter;

import processing.core.PApplet;
public class StrokeField {
	int F, G;
	FVector [] field;
	StrokeField( int _F, int _G)
	{
		F = _F; G = _G;
		field = new FVector[(F+1)*(G+1)];
		for(int i = 0; i < (F+1)*(G+1); i++)
			field[i] = new FVector(0,0);
	}
	public void draw(PApplet p)
	{
		p.pushMatrix();
		//p.translate(-p.WIDTH/2f,-p.HEIGHT/2f);
		//p.scale(xScale,yScale);
		//p.translate((p.WIDTH/2f - width/2f)/xScale, (p.HEIGHT/2f - height/2f)/yScale);
		//p.translate((p.WIDTH/2f)/xScale,p.HEIGHT/2f/yScale);
		p.stroke(255,0,0);
		for(int i = 0; i <= F; i++)
		{
			for(int j = 0; j <= G; j++)
			{
				int x = (int) ((float)Smain.WIDTH * i /(float)F);
				int y = (int) ((float)Smain.HEIGHT * j/(float)G);
				p.line(x,y,x+field[j*(F+1)+i].x,y+field[j*(F+1)+i].y);
			}
		}
		p.popMatrix();
	}
	float phi4(float x)
	{
		float mag = Math.abs(x);
		if(mag > 2)
			return 0;
		else if ( mag > 1)
			return (float) ((1/8f) * (5-2*mag - Math.sqrt(-7 + 12*mag - 4 * mag * mag)));
		else
			return (float) ((1/8f) * (3-2*mag + Math.sqrt(1 + 4*mag - 4 * mag * mag)));
	}
	private int round(float a) {
		return (int)Math.floor(a + 0.5f);
	}
	float delta4(FVector pos)
	{

		float x = pos.x;
		float y = pos.y;
		//PApplet.println(x + " " + phi4(x));
		return phi4(x)*phi4(y);
		//return (1/(1/(F+1))/(1/(G+1)))*phi4(x)*phi4(y);
	}
	int getIndices(FVector pos)
	{
		int x = round(pos.x);
		int y = round(pos.y);
		return y*(F+1) + x;
	}
	private List<Integer> getNeighboringIndices(FVector pos)
	{
		//pos is assumed to be in F+1 x G+1 grid
		LinkedList<Integer> indices = new LinkedList<Integer>();
		int support = 2;
		for(int i = 0; i < support; i++)
		{
			//swap F and G here?
			//go left of support
			if(pos.x-i >= 0)
			{
				for(int j = 0; j < support; j++ )
				{	
					if(pos.y-i >= 0)
						indices.add(new Integer(getIndices(pos.add(new FVector(-i,-j)))));
					if(pos.y +i < G+1)
						indices.add(new Integer(getIndices(pos.add(new FVector(-i,j)))));
				}
			}
			if(pos.x+i < F+1)
			{
				for(int j = 0; j < support; j++ )
				{
					if(pos.y-i >= 0)
						indices.add(new Integer(getIndices(pos.add(new FVector(i,-j)))));
					if(pos.y +i < G+1)
						indices.add(new Integer(getIndices(pos.add(new FVector(i,j)))));
				}
			}
		}
		return indices;
	}
	public void stroke(PApplet p,FVector start, FVector end)
	{
		stroke(p,start,end,20,1);
	}
	public void stroke(PApplet p, FVector start, FVector end, int steps, float pressure)
	{
		for(int i = 0; i <= steps; i++)
		{
			//p = start(1-lambda) + end(lambda)
			//FVector pt = start.multiply(1-i/(float)steps).add(end.multiply(i/(float)steps));
			FVector pt = new FVector(end);
			//scale pt to be in 1x1 grid
			pt.x /=(float)Smain.WIDTH;
			pt.y /= (float)Smain.HEIGHT;
			
			//PApplet.println(pt.x + " " + pt.y);
			//compute neighboring points of grid around p
			//rescale pt to be in F+1 x G+1 grid
			pt.x *= F+1;
			pt.y *= G+1;
			
			for(Integer e : getNeighboringIndices(pt))
			//for(int e = 0; e < (F+1)*(G+1); e++)
			{
				//TODO compute o
				FVector o = end.add(start.multiply(-1)).multiply(((1/(float)steps)/(1/(float)(G+1))/(1/(float)(F+1))));
				//p.println(pt.add(gridVector(e).multiply(-1)));
				//p.println(delta4(pt.add(gridVector(e).multiply(-1))));
				o = o.multiply(0.005f*delta4(pt.add(gridVector(e).multiply(-1))));
				field[e] = field[e].capAdd(o,100);
			}
		}
	}
	public void writeFile(FileOutputStream file)
	{
		DataOutputStream s = new DataOutputStream(file);
		try{
			s.writeInt(F+1);
			s.writeInt(G+1);//8bytes total
			for(int i = 0; i < field.length; i++)
			{
				s.writeFloat(field[i].x);
				s.writeFloat(field[i].y);
			}
		}
		catch(Exception e)
		{
		}
		finally
		{
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	private FVector gridVector(Integer e) {
		//returns the position of the grid
		return new FVector(e%(G+1), e/(F+1));
	}
}
