package main;

public class FVector {
	public float x,y;
	public FVector(){ x = 0; y = 0;}
	public FVector(float _x, float _y)
	{
		x = _x; y = _y;
	}
	public FVector(FVector o)
	{
		x = o.x; y = o.y;
	}
	public float magnitude()
	{
		return (float) Math.sqrt(x*x+y*y);
	}
	public FVector capAdd(FVector o, float cap)
	{
		FVector r = this.add(o);
		float m = r.magnitude();
		if(m > cap)
			r = r.divide(m).multiply(cap);
		return r;
	}
	public FVector add( FVector o )
	{
		return new FVector(x+o.x,y+o.y);
	}
	public FVector divide( float f )
	{
		return new FVector(x/f,y/f);
	}
	public FVector multiply( float f )
	{
		return new FVector(x*f,y*f);
	}
	public String toString()
	{
		return "FV " + x + " " + y;
	}
}
