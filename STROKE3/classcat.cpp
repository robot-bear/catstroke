#include "APMain.h"
#include "QuGlobals.h"
#include "classcat.h"
#include "QuInterface.h"
#include "QuSoundManager.h"
#include "QuConstants.h"
#include "s3eKeyboard.h"

#include "s3eVibra.h"


void StrokeableNewMenu::stroke(float sx, float sy, float ex, float ey, float t)
{
	//stroke each field
	mStroke->setField(f1);
	mCat1.stroke(sx,sy,ex,ey,t);
	v1 = mCat1.getScore();

	if(v1 < MENU_HISS_THRESHOLD)
		;//TODO play a sound
}

QuStupidPointer<StrokeableObject> StrokeableNewMenu::update()
{
	mCat1.update();

	v1 = mCat1.getScore();
	std::cout << v1 << std::endl;
	//v1 = 999999999;
	if(v1 > MENU_STROKE_TRIGGER_THRESHOLD)
	{
		reset();
		QuStupidPointer<StrokeableCat> c(new StrokeableCat(p));
		c->setCat(p->getLoader().getNextCat());
		return c.safeCast<StrokeableObject>();
	} 
	else
		return QuStupidPointer<StrokeableObject>();


}

//DELETE ME
void StrokeableMenu::stroke(float sx, float sy, float ex, float ey, float t)
{
	//stroke each field
	mStroke->setField(f1);
	mCat1.stroke(sx,sy,ex,ey,t);
	mStroke->setField(f2);
	mCat2.stroke(sx,sy,ex,ey,t);
	mStroke->setField(f3);
	mCat3.stroke(sx,sy,ex,ey,t);

	v1 = mCat1.getScore();
	v2 = mCat2.getScore();
	v3 = mCat3.getScore();

	if(v1 < MENU_HISS_THRESHOLD || v2 < MENU_HISS_THRESHOLD || v3 < MENU_HISS_THRESHOLD)
		;//TODO play a sound
}

//DELETE ME, no more menu cat
QuStupidPointer<StrokeableObject> StrokeableMenu::update()
{
	mCat1.update();
	mCat2.update();
	mCat3.update();

	v1 = mCat1.getScore();
	v2 = mCat2.getScore();
	v3 = mCat3.getScore();

	v1 = 999999999;
	//std::cout << v1 << " " << v2 << " " << v3 << std::endl;
	if(v1 > MENU_STROKE_TRIGGER_THRESHOLD)
	{
		reset();
		QuStupidPointer<StrokeableCat> c(new StrokeableCat(p));
		c->setCat(p->getLoader().getNextCat());
		return c.safeCast<StrokeableObject>();
	} 
	else if (v2 > MENU_STROKE_TRIGGER_THRESHOLD)
	{
		reset();
		return QuStupidPointer<StrokeableCredits>(new StrokeableCredits(p,this)).safeCast<StrokeableObject>();
	}
	else if (v3 > MENU_STROKE_TRIGGER_THRESHOLD)
		return QuStupidPointer<ExitCat>(new ExitCat(p)).safeCast<StrokeableObject>();
	else
		return QuStupidPointer<StrokeableObject>();


}

StrokeableCat::StrokeableCat(CatApp * _p):
	StrokeableObject(_p),
	mScore(0),
	mLast(0,IMPATIENT_CAT_TIME_FIRST),
	mSignal(CAT_MEMORY),
	mDistance(200)
{
	mSoundProfile = p->getSoundLoader().getRandomProfile();
}

void StrokeableCat::stroke(float sx, float sy, float ex, float ey, float t)
{
	float strokeVal = mCat->stroke(sx,sy,ex,ey,t);
	if(strokeVal != strokeVal)
	{
		std::cout << "QNAN detected" << std::endl;
		mCat->stroke(sx,sy,ex,ey,t);
		strokeVal = 0;
	}
	mSignal.add(strokeVal,t);
	float speed = mDistance.weightedAverage(200);
	if(speed == 0) 
		speed = 0.020888f;
	mScore = quClamp<float>(strokeVal/speed/400.0f+mScore,0,9999999999999999.0f);
	float distance = sqrt((ey-sy)*(ey-sy) + (ex-ey)*(ex-ey));
	mDistance.add(distance,t);
	lastMovePos = QuVector2<float>(ex, ey);
}

QuStupidPointer<StrokeableObject> StrokeableCat::update()
{
		
	if(mLoseTimer.isSet())
	{
		if(mLoseTimer.isExpired())
		{
			//this is for the menu case, where it *should* play the slashes and then reset the score 
			reset();
			return QuStupidPointer<StrokeableObject>(new ExitCat(p));
		}
		mLoseTimer.update();
	}

	float trav = mDistance.weightedSum();
	float diff = mSignal.weightedAverage(200); //average
	float sum = mScore;
	float negSum = mSignal.weightedSum();
	//std::cout << "the diff: " <<  diff << std::endl;
	if( trav > 0)
	{
		if(quAbs(diff) > 0.1)
		{
			QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(G_DEVICE_ROTATION);
			//note something unintended may happen by passing  in sum instead of negSum but nothingh I can notice so I'll leave it as is
			mFx.stroked(lastMovePos.x*dim.x,lastMovePos.y*dim.y,diff, sum,trav);
			//this could be tricky because soemtimes there is a micro negative score thing
			//TODO should keep one more counter that does the average stroke of the last 100 ms
			mSound.stroke(diff,mSoundProfile);
		}
	}
	else
		mSignal.add(0,G_GAME_GLOBAL.getFramerate());

	mDistance.reset();
	mFx.update();

	

	if(sum >= CAT_WIN)
	{
		std::cout << "you beat this level" << std::endl;
		QuStupidPointer<StrokeableCat> c(new StrokeableCat(p));
		c->setCat(p->getLoader().getNextCat());
		if(c->mCat.isNull())
			return QuStupidPointer<StrokeableObject>(new WinCat(p));
		else
			return c.safeCast<StrokeableObject>();
	}
	else if (negSum <= CAT_LOSE)
	{
		std::cout << sum << std::endl;
		std::cout << "you have lost!" << std::endl;
		mLoseTimer.setTarget(45);
	}
	return QuStupidPointer<StrokeableObject>();
}

void StrokeableCat::draw()
{
	if(mLoseTimer.isSet())
	{
		
		QuShapeDrawer drawer;
		GDeviceRotation rot = G_DEVICE_ROTATION;
		if(rot != G_DR_270)
			rot = G_DR_90;
		float k = mLoseTimer.getLinear();
		float f1 = pow(1-k/0.7,4.0);
		float f2 = k > 0.3 ? pow(1-(k-0.3)/0.7,4.0) : 0;
		mCat->draw(quClamp<float>(quMax(f1,f2),0,1));
		mFx.draw();
		if(k < 0.7)
			drawer.drawImageInRectangle(QuBox2<float>(0,0,1,1), G_IMAGE_MANAGER.getFlyImage("images/slash.png"),f1);
		if(k > 0.3)
			drawer.drawImageInRectangle(QuBox2<float>(0,0,1,1), G_IMAGE_MANAGER.getFlyImage("images/slashback.png"),f2);

		if(mLoseTimer.isTriggered(0))
		{
			G_SOUND_MANAGER.playSound("sounds/slash.raw");
			s3eVibraVibrate(255,100);
		}
		if(mLoseTimer.isTriggered(45*0.3))
		{
			G_SOUND_MANAGER.playSound("sounds/slash2.raw");
			s3eVibraVibrate(255,100);
		}
	}
	else
	{
		mCat->draw();
		mFx.draw();
	}
}

void CatApp::initialize()
{
	//sloppy TODO get rid of this and make a better system for loading images. for one thing, you could make G_IMAGE_MANAGER only handle pngs I mean srsly.
	((QuPngImage *)&(G_IMAGE_MANAGER.setFlyImage("images/slash.png")))->pumpImage(99999999999);
	((QuPngImage *)&(G_IMAGE_MANAGER.setFlyImage("images/slashback.png")))->pumpImage(99999999999);
	//((QuPngImage *)&(G_IMAGE_MANAGER.setFlyImage("images/losecat.png")))->pumpImage(99999999999);
	//((QuPngImage *)&(G_IMAGE_MANAGER.setFlyImage("images/catmaster.png")))->pumpImage(99999999999);
	G_SOUND_MANAGER.loadSound("sounds/slash.raw");
	G_SOUND_MANAGER.loadSound("sounds/slash2.raw");
}

void CatApp::setup()
{
}

void CatApp::drawDummyLoadingImage()
{
	QuTintableGuiImage img;
	img.loadImage(std::string("images/image.png"));
	img.draw();
}

void CatApp::update()
{
	if(hasLoaded == 1)
	{
		initialize(); hasLoaded++;
	}
	else if(hasLoaded == 2)
	{
		loader.update();	
		QuStupidPointer<StrokeableObject> n = man->update();
		if(!n.isNull())
			man = n;
		fireworks.update();
	}
}

void CatApp::draw(){
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	if(hasLoaded == 0)
	{
		drawDummyLoadingImage(); 
		hasLoaded++;
		return;
	}
	man->draw();
	fireworks.draw();
}

void CatApp::keyPressed(int key){
}

void CatApp::keyReleased(int key){

	if(key == s3eKey0)
	{
		QuStupidPointer<StrokeableCat> c(new StrokeableCat(this));
		c->setCat(getLoader().getNextCat());
		man = c.safeCast<StrokeableObject>();
	}
}
 
void CatApp::mousePressed(QuScreenCoord crd, int button)
{

	isCursorDown = true;
	timeSinceLastMouseMove = G_GAME_GLOBAL.getMillis();
	crd.rotate(G_DEVICE_ROTATION);
	lastPos = crd.getFPosition();
	//std::cout << lastPos.x << " <-x y-> " << lastPos.y << std::endl;
}

void CatApp::mouseReleased(QuScreenCoord crd, int button)
{
	isCursorDown = false;
}

void CatApp::mouseMoved(QuScreenCoord crd)
{
	crd.rotate(G_DEVICE_ROTATION);
	QuVector2<float> p = crd.getFPosition();
	if(isCursorDown && lastPos != p)
	{
		int t = G_GAME_GLOBAL.getMillis();
		int dt = G_GAME_GLOBAL.getMillis() - timeSinceLastMouseMove;
		timeSinceLastMouseMove = t;
		man->stroke(lastPos.x,lastPos.y,p.x,p.y,dt);
		lastPos = crd.getFPosition();
		QuVector2<int> ip = crd.getIPosition();
		//fireworks.stroke(ip.x,ip.y,2);
	}
}

void CatApp::tiltUpdate()
{
	float y = s3eAccelerometerGetY()/1000.0;
	float x = s3eAccelerometerGetX()/1000.0;
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x*=-1; y*=-1;
	}
	//mCam->setTiltTarget(mAccel.getY(),mAccel.getX());
	//mCam->adjustTiltTarget(-mMouse.getChange().x/100.,mMouse.getChange().y/200.);
}

ExitCat::ExitCat(CatApp * _p):StrokeableObject(_p),mQuitTimer(0,150)
{
	p->getLoader().unloadAllCats();
	
}

WinCat::WinCat(CatApp * _p):StrokeableObject(_p),mQuitTimer(0,150)
{
	p->getLoader().unloadAllCats();
}

int main()
{
	QuStupidPointer<QuGlobalSettings> s = new QuGlobalTypeSettings<QuApRawSoundManager<44100>,QuImageManager,ApFileManager>();
	INITIALIZE_GAME<CatManager>(s);
}


