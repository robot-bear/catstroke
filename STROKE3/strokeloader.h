#pragma once
#include "s3eFile.h"
#include "s3eDebug.h"
#include <string>
#include <vector>
#include "s3eMemory.h"
#include "QuImage.h"
#include "QuConstants.h"
#include "QuCamera.h"
#include "QuDrawer.h"
#include "QuCMath.h"
#include <cstring>

typedef QuBasicCamera CatCamera;
struct StrokeField
{
	int F,G;
	QuStupidPointer<float> mData;
	StrokeField(QuStupidPointer<float> aData, int aF, int aG):mData(aData),F(aF),G(aG)
	{

	}
	~StrokeField()
	{
	}
	float phi4(float x)
	{
		float mag = quAbs(x);
		if(mag > 2)
			return 0;
		else if ( mag > 1)
			return (float) ((1/8.0) * (5-2*mag - sqrt(-7 + 12*mag - 4 * mag * mag)));
		else
			return (float) ((1/8.0) * (3-2*mag + sqrt(1 + 4*mag - 4 * mag * mag)));
	}
	float delta4(float x, float y)
	{
		return phi4(x)*phi4(y);
	}
	//x,y assumed to be in [0,F], [0,G]
	void deltaInterpolate(float x, float y, float & ax, float & ay)
	{
		ax = ay = 0;
		int support = 2;
		for(int i = -support; i <= support; i++)
		{
			for(int j = -support; j <= support; j++ )
			{	
				if(x+i < 0 || x+i > F || y+j < 0 || y+j > G)
					continue;
				
				//we do it
				int rx = (int)round(x+i);
				int ry = (int)round(y+j);
				float weight = delta4(rx-x,ry-y);
				if(weight > 0 && mData[2*(ry*F + rx)] > 0)
				{
					quDummy();
				}
				ax += weight * mData[2*(ry*F + rx)];
				ay += weight * mData[2*(ry*F + rx)+1];
			}
		}
	}
	float stroke(float sx, float sy, float ex, float ey, float ir, float dt)
	{
		StrokeField * field = this;
		//float sr = G_SCREEN_WIDTH/(float)G_SCREEN_HEIGHT;
		QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(G_DEVICE_ROTATION);
		float sr = dim.x/(float)dim.y;

		if(sr >= ir)
		{
			//scale the height cuz it got changed
			sy = (sy-0.5) * ir / sr + 0.5;
			ey = (ey-0.5) * ir / sr + 0.5;
		}
		else
		{
			sx = (sx-0.5) * sr / ir + 0.5;
			ex = (ex-0.5) * sr / ir + 0.5;
		}

		sx *= field->F;
		ex *= field->F;
		sy *= field->G;
		ey *= field->G;
		 
		//need to rotate by 90 degrees CW
		QuScreenCoord s(sx,sy,field->F,field->G);
		QuScreenCoord e(ex,ey,field->F,field->G);
		s.rotate(G_DR_90);
		e.rotate(G_DR_90);

		sx = s.getIPosition().x;
		sy = s.getIPosition().y;
		ex = e.getIPosition().x;
		ey = e.getIPosition().y;

		float dx = ex-sx; 
		float dy = ey-sy;
		float mag = (float)sqrt(dx*dx + dy*dy);
		if(mag == 0) return 0;
		//take the dot product against our delta function with N steps;
		int N = 10;
		float r = 0;
		for(int i = 0; i < N; i++)
		{
			float cx = sx + dx*i/(float)N;
			float cy = sy + dy*i/(float)N;	
			r += field->stroke(cx,cy,dx,dy,dt) * mag / (float)N; 
		}
		return r * 10.0f;
	}
private:
	float stroke(float x, float y, float dphix, float dphiy, float dt)
	{
		float ax, ay;
		deltaInterpolate(x,y,ax,ay);
		//TODO scale dphix and dphiy
		//std::cout << "x, y, phix, phiy: " << ax << " " << ay << " " << dphix << " " << dphiy << std::endl;
		//return 1-QuFVector2<float>(dphix*dt/30.0f-ax/30.0f,dphiy*dt/30.0f-ay/30.0f).magnitude();
		
		float norma = sqrt(ax*ax + ay*ay);
		float normb = sqrt(dphix*dphix + dphiy*dphiy);
		float normab = norma*normb;
		if(normab == 0)
			return 0;
		float dot = dphix*ax+dphiy*ay;
		float theta = acos(dot/normab);
		//theta = sqrt(quAbs(theta/PI))*PI*quSgn(theta);
		theta = pow(quAbs(theta/PI),2/3.0)*PI*quSgn(theta);
		
		//TODO broken :(
		float btild = ROOT_TWO*dt*normb/100.0f;
		float rat = quMin(norma, btild)/quMax(norma,btild);
		rat = 1;
		
		if(theta > 0)
			return cos(theta)*normab*rat;
		else
			return cos(theta)*normab;

		//return (dphix*ax+dphiy*ay);
		
	}
};


class QuDrawingHelper
{
public:
	static void drawObjectToScale(QuDrawObject & draw, float scalex, float scaley)
	{
		CatCamera camera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT);
		camera.setRotation(G_GAME_GLOBAL.getDeviceRotation(),0);

		glPushMatrix();
		camera.setScene();
		glScalef(scalex,scaley,1);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);

		//draw it here
		draw.autoDraw();

		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glDisable(GL_BLEND);
		glPopMatrix();
	}
	static void drawObjectToFitScreen(QuDrawObject & img, float shake = 0)
	{
		CatCamera camera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT);

		//camera.setRotation(G_DR_90);
		camera.setRotation(G_DEVICE_ROTATION);

		QuVector2<int> dim(camera.getRotatedWidth(),camera.getRotatedHeight());
		float sr = dim.x/(float)dim.y;
		float ir = img.getImageWidthToHeightRatio();

		glPushMatrix();
		camera.setScene();
		
		if(sr >= ir)
			glScalef(dim.x,dim.x/ir,1);	
		else
			glScalef(dim.y*ir,dim.y,1);

		if(shake)
			glTranslatef(quRandfSym()*shake/60.0f,quRandfSym()*shake/60.0f,0);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);

		//draw it here
		img.autoDraw();

		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glDisable(GL_BLEND);
		glPopMatrix();
		
	}
};

inline void drawImageToFitScreen(QuDrawObject & img, StrokeField * field = NULL, float shake = 0)
{
	QuDrawingHelper::drawObjectToFitScreen(img,shake);
	if(field != NULL)
	{
		QuDrawObject f;
		int vertexCount = field->F*field->G*2;
		f.setCount(vertexCount+4);
		f.setDrawType(GL_LINES);
		float * verts = new float[vertexCount*2+4];

		//x and y are swapped in the dat file
		//set verts
		//make vertices in [0,1]x[0,1] type coordinates
		for(int i = 0; i < field->F; i++)
		{
			for(int j = 0; j < field->G; j++)
			{
				float ax,ay;
				field->deltaInterpolate(i,j,ax,ay);
				int s = (i*field->G + j)*4;
				
				verts[s + 1] = i/(float)field->F - 0.5f;
				verts[s + 0] = j/(float)field->G - 0.5f;
				verts[s + 3] = verts[s + 1] + ax*0.003;
				verts[s + 2] = verts[s + 0] + ay*0.003;
				
			}
		}

		//diagonal vert
		verts[vertexCount*2] = -0.5;
		verts[vertexCount*2 + 1] = -0.5;
		verts[vertexCount*2 + 2] = 0.5;
		verts[vertexCount*2 + 3] = 0.5;
		f.loadVertices(verts,2);


		
		QuBasicCamera camera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT);
		camera.setRotation(G_DEVICE_ROTATION);
		QuVector2<int> dim(camera.getRotatedWidth(),camera.getRotatedHeight());
		float sr = dim.x/(float)dim.y;
		float ir = img.getImageWidthToHeightRatio();

		//also reflect it so it is correct
		if(sr >= ir)
			QuDrawingHelper::drawObjectToScale(f,-dim.x,-dim.x/ir);	
		else
			QuDrawingHelper::drawObjectToScale(f,-dim.y*ir,-dim.y);
		
		/*
		float ir = img.getImageWidthToHeightRatio();
		float sr = G_SCREEN_WIDTH/(float)G_SCREEN_HEIGHT;

		//negative sign below is because the stroke field drawer gets flipped I'm not quite sure why
		//but between the level editor and the game itself, the stroking is correct
		//maybe  I made two wrongs or something. who knows, lets not worry about it
		//whatever mate.
		if(sr >= ir)
			QuDrawingHelper::drawObjectToScale(f,G_SCREEN_WIDTH,-G_SCREEN_WIDTH*ir);
		else
			QuDrawingHelper::drawObjectToScale(f,G_SCREEN_HEIGHT/ir,-G_SCREEN_HEIGHT);
		*/
	}
}

class StrokeObject
{
	std::string mFilename;
	bool isInit;
	QuStupidPointer<StrokeField> field;
	QuColorableDrawObject<int> mImage;
	QuArrayInterpolator<float,4> mColor;
public:
	StrokeObject():isInit(false),field(NULL)
	{
		mColor.setBaseValue(NEUTRAL_COLOR);
		mColor.setTargetValue(NASTY_COLOR);
	}
	~StrokeObject()
	{
		mImage.unloadImage();
		//TODO you were using this image as a test image so you can't delete it lolo.
		//G_IMAGE_MANAGER.deleteFlyImage(mFilename);
	}
	bool pump(int lines)
	{
		if(!mImage.getImage().isNull())
		{
			//TODO get rid of me
			return ((QuPngImage *)&(mImage.getImage()))->pumpImage(lines);
		}
		return false;
	}
	bool isReady()
	{
		if(!isInit)
			return false;
		return mImage.isTextureLoaded();
	}

	//TODO color parameters
	void draw(float shake = 0)
	{
		if(!isReady())
			return;

		/* do something like this
		float * fourc = cpcpcpVector<float>(mColor.interpolate(mTimer.getLinear()).d,4,4);
		mDrawObject.addColor(1,fourc,4);
		mDrawObject.setKey(1);*/
		//mImage.addColor(1,COLORPOINTER,4);
		//mImage.setKey(1);

		//drawImageToFitScreen(mImage,field.rawPointer());
		drawImageToFitScreen(mImage,NULL,shake);
	}
	//x and y should be in [0,1] which we map to [0,F] and [0,G] respectively
	//TODO this needs to rescale the input so it gets cropped to the area of the image onthe screen
	float stroke(float sx, float sy, float ex, float ey, float dt)
	{
		return field->stroke(sx,sy,ex,ey,mImage.getImageWidthToHeightRatio(),dt);
	}
	static float * loadField(std::string filename, long & F, long & G)
	{
		s3eFile * datf = s3eFileOpen(filename.c_str(),"rb");
		if(!datf)
		{
			s3eDebugTracePrintf("dat file loading failed");
			return false;
		}
		char bytes[4];
		s3eFileRead(bytes,4,1,datf);
		F = (long(bytes[0]) << 24) | (long(bytes[1]) << 16) |
        (long(bytes[2]) << 8)  |  long(bytes[3]);
		s3eFileRead(bytes,4,1,datf);
		G = (long(bytes[0]) << 24) | (long(bytes[1]) << 16) |
        (long(bytes[2]) << 8)  |  long(bytes[3]);
		//double check oursize is correct
		if(F*G*2*4 != s3eFileGetSize(datf)-8)
		{
			s3eDebugTracePrintf("dat file not formated correctly, F = %i, G = %i, F*G = %i, filesize-8 = %i",F,G,F*G*2*4,s3eFileGetSize(datf)-8);
			s3eFileClose(datf);
			return false;
		}
		float * data = new float[F*G*2*sizeof(float)];
		//TODO need to actually read in the file lolcakes
		s3eFileRead(data,sizeof(float),F*G*2,datf);
		//reorder it
		
		char * f = (char *)data;
		for(int i = 0; i < F * G * 2; i++)
		{
			quSwap<char>(f[4*i],f[4*i+3]);
			quSwap<char>(f[4*i+1],f[4*i+2]);
		}

		s3eFileClose(datf);
		return data;
	}
	bool init(std::string aFilename)
	{
		G_GAME_GLOBAL.dlog("loading file");
		mFilename = aFilename;
		std::string dat = mFilename.substr(0,mFilename.length() - 4) + ".dat";
		long F = 0;
		long G = 0;
		float * data = loadField(dat,F,G);
		field = QuStupidPointer<StrokeField>(new StrokeField(QuStupidPointer<float>(data,-1),F,G));
		std::cout << "field F " << F << " G " << G << " loaded" << std::endl;

		//now we load the image
		mImage.setCount(4);
		QuStupidPointer<QuBaseImage> img = G_IMAGE_MANAGER.setFlyImage(mFilename);
		isInit = mImage.loadImage(img);


		//TODO set texture coord stuff so it is rotated by 90
		//set the vertex array info float arr[] = {0,0,0,h,w,0,w,h};
		//set the texture
		float w = img->getWidth()/(float)img->getPotW();
		float h = img->getHeight()/(float)img->getPotH(); 
		//float arr[] = {0,0,0,h,w,0,w,h};
		float arr[] = {0,0,w,0,0,h,w,h};
		mImage.loadTexCoords(ARRAYN<float,8>(arr));


		/*
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mImage.loadVertices(rectCoords);*/
		mImage.loadVertices(ARRAYN<float,8>::conv2<4,3>(GUI_RECT_COORDS));

		return isInit;
	}
	//hack
	void setField(QuStupidPointer<StrokeField> _f)
	{
		field = _f;
	}
};


struct SoundProfile
{
	std::vector<std::string> mMeow;
	std::vector<std::string> mHiss;
	void playMeow()
	{
		if(mMeow.size() > 0)
			G_SOUND_MANAGER.getSound(mMeow[quRandRange(0,mMeow.size()-1)])->play();
	}
	void playHiss()
	{
		if(mHiss.size() > 0)
			G_SOUND_MANAGER.getSound(mHiss[quRandRange(0,mHiss.size()-1)])->play();
	}
	void unloadAllSounds()
	{
		for(int i = 0; i < mMeow.size(); i++)
		{
			G_SOUND_MANAGER.safeDeleteSound(mMeow[i]);
		}
		for(int i = 0; i < mHiss.size(); i++)
		{
			G_SOUND_MANAGER.safeDeleteSound(mMeow[i]);
		}
	}
};

class SoundProfileLoader
{
	std::vector<QuStupidPointer<SoundProfile> > mProfiles;

	bool hasExtension(char * fn)
	{
		if( strchr(fn,'.') == NULL )
			return false;
		return true;
	}

	std::string getExtension(std::string fn)
	{
		int index = fn.find_last_of(".");
		if(index == -1)
			return "";
		return fn.substr(index, fn.length()-index);
	}

	void loadSounds()
	{
		srand(time(NULL));
		//srand(3);
	

		//TODO needs to randomize list of cats
		s3eFileList * dir = s3eFileListDirectory("soundprofiles");
		if(dir == NULL)
		{
			std::cout << "directory list failed" << std::endl;
			s3eFileError e = s3eFileGetError();
		}
		std::vector<std::string> temp;
		char * fn = new char[1024];
		while(s3eFileListNext(dir,fn,1024) == S3E_RESULT_SUCCESS)
		{
			if(!hasExtension(fn))
			{
				s3eFileList * subdir = s3eFileListDirectory( ("soundprofiles/" + std::string(fn)).c_str() );
				char * fn2 = new char[1024];
				if(dir != NULL)
				{
					QuStupidPointer<SoundProfile> profile = new SoundProfile();
					mProfiles.push_back(profile);
					while(s3eFileListNext(subdir,fn2,1024) == S3E_RESULT_SUCCESS)
					{
						std::string str = std::string(fn);
						std::string fn2string(fn2);
						std::cout << "grabbing sound " << "soundprofiles/" + str + "/" + std::string(fn2) << std::endl;
						if(getExtension(str) == ".raw");
						{
							if((int)fn2string.find("meow") != std::string::npos)
								profile->mMeow.push_back("soundprofiles/" + str + "/" + std::string(fn2));
							else if((int)fn2string.find("hiss") != std::string::npos)
								profile->mHiss.push_back("soundprofiles/" + str + "/" + std::string(fn2));
						}

					}
				}
				delete [] fn2;
				s3eFileListClose(subdir);
			}
		}
		delete [] fn;
	}

public:
	SoundProfileLoader()
	{
		loadSounds();
	}
	QuStupidPointer<SoundProfile> getRandomProfile()
	{
		return mProfiles[quRandRange(0,mProfiles.size() -1)];
	}
};

class StrokeLoader
{
public:
	std::vector<std::string> fileList;
	int mIndex;
	QuStupidPointer<StrokeObject> currentCat;
	QuStupidPointer<StrokeObject> nextCat;
	~StrokeLoader()
	{
	}
	//some memory issues with crappy iphones so this should help out there...
	void unloadAllCats()
	{
		currentCat.setNull();
		nextCat.setNull();
	}
	QuStupidPointer<StrokeObject> & getCurrentCat()
	{
		return currentCat;
	}
	QuStupidPointer<StrokeObject> getNextCat()
	{

		if(fileList.size() == 0)
		{
			return QuStupidPointer<StrokeObject>();
		}
		if(!currentCat.isNull())
			currentCat.setNull();
		currentCat = nextCat;
		if(!currentCat.isNull())
			currentCat->pump(99999999);
		if(mIndex <= fileList.size())
		{
			if(mIndex < fileList.size())
			{
				nextCat = QuStupidPointer<StrokeObject>(new StrokeObject());
				nextCat->init(fileList[mIndex]);
			}
			mIndex++;
			if(currentCat.isNull())
				return getNextCat();
			return currentCat;
		}
		else
			return QuStupidPointer<StrokeObject>(NULL);
	}
	void update()
	{
		if(!nextCat.isNull())
			nextCat->pump(20);
	}
	StrokeLoader():mIndex(0)
	{
		srand(time(NULL));
		//srand(3);
	

		//TODO needs to randomize list of cats
		s3eFileList * dir = s3eFileListDirectory("data");
		if(dir == NULL)
		{

			std::cout << "directory list failed" << std::endl;
			s3eFileError e = s3eFileGetError();
			switch(e)
			{
			case S3E_FILE_ERR_NOT_FOUND:
				std::cout << "one" << std::endl; break;
			case S3E_FILE_ERR_DEVICE:
				std::cout << "two" << std::endl; break;
			case S3E_FILE_ERR_TOO_MANY:
				std::cout << "three" << std::endl; break;
			case S3E_FILE_ERR_MEM:
				std::cout << "four" << std::endl; break;
			default:
				std::cout << "fail" << std::endl; break;
			}
			std::cout << "after fail" << std::endl;
			return;
		}
		std::vector<std::string> temp;
		char * fn = new char[1024];
		while(s3eFileListNext(dir,fn,1024) == S3E_RESULT_SUCCESS)
		{
			std::string str = std::string(fn);
			if(str.substr(str.length()-4, 4) == ".dat")
			{
				//s3eDebugTracePrintf("Found dat file %c", fn);
				continue;
			}
			temp.push_back("data/" + std::string(fn));
		}
		delete [] fn;
		while(temp.size() > 0)
		{
			int index = rand()%temp.size();
			fileList.push_back(temp[index]);
			temp.erase(temp.begin()+index);
		}
		std::cout << "found this many cats " << fileList.size() << std::endl;
	}
private:
};