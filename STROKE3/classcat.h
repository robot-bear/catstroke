#pragma once
#include "QuUtils.h"
#include "QuCamera.h"
#include "QuInterface.h"
#include "QuGameManager.h"
#include "strokeloader.h"
#include "catconstants.h"
#include "QuParticles.h"
#include "QuButton.h"
#include <stdint.h>
#include "s3eVibra.h"


class CatApp;

class StrokeSound
{
	QuStupidPointer<QuSoundStructInterface> mSound;
	uint64 mLastSoundPlayed;
	uint64 mLastVibra;
public:
	StrokeSound()
	{
		setup();
	}
	void setup()
	{
		//G_SOUND_MANAGER.loadSound("sounds/FIRE.raw",44100);
		mLastSoundPlayed = G_GAME_GLOBAL.getMillis();
	}
	void needy(float v)
	{
		/*
		if(!G_SOUND_MANAGER.areSoundsPlaying())
		{
			//TODO play needy sound based on catscore v
		}*/
	}
	void slash()
	{
		G_SOUND_MANAGER.playSound("sounds/slash.raw");
	}
	void stroke(float v, QuStupidPointer<SoundProfile> p)
	{
		if(!G_SOUND_MANAGER.areSoundsPlaying())
		{
			//std::cout << G_GAME_GLOBAL.getMillis() << " " << mLastSoundPlayed << std::endl;
			if(v > 0 && G_GAME_GLOBAL.getMillis() > mLastSoundPlayed)
			{
				p->playMeow();
				mLastSoundPlayed = G_GAME_GLOBAL.getMillis() + quRandRange(1000,6000);
				s3eVibraVibrate(255,quRandRange(300,800));
			}
			if(v < 0)
				p->playHiss();
		}

		if( v < 0 )
			s3eVibraVibrate(255,200);
		/*
		//TODO this should look at the aggregate catScore in the last x frames
		if(!G_SOUND_MANAGER.areSoundsPlaying())
		{
			//TODO should sort by degree of score
			if(v > 0)
				G_SOUND_MANAGER.playSound(getRandomHappySound());
			if(v < 0)
				G_SOUND_MANAGER.playSound(getRandomUnhappySound());
		}*/
	}
private:
	/*
	void play(std::string aSound)
	{
		mSound = G_SOUND_MANAGER.getSound(aSound);
		mSound->play();
	}
	std::string getRandomHappySound()
	{
		return HAPPY_CAT_SOUNDS[quRandRange(0,NUMBER_HAPPY)];
	}
	std::string getRandomNeedySound()
	{
		return NEEDY_CAT_SOUNDS[quRandRange(0,NUMBER_NEEDY)];
	}
	std::string getRandomUnhappySound()
	{
		return NOT_HAPPY_CAT_SOUNDS[quRandRange(0,NUMBER_UNHAPPY)];
	}*/
};

class StrokeEffects
{
	QuParticleManager fireworks;
	QuArrayInterpolator<float,4> inter1;
	QuArrayInterpolator<float,4> inter2;
	QuStupidPointer<QuTintableGuiImage> mImg1;
	QuStupidPointer<QuTintableGuiImage> mImg2;
	QuStupidPointer<QuTintableGuiImage> mImg3;
	QuStupidPointer<QuTintableGuiImage> mImgUni;
private:
	float threeint(float v1, float v2, float v3, float x1, float x2, float x3, float x)
	{
		if( x < x2 )
		{
			float theta = quClamp<float>((x-x1)/(x2-x1),0,1);
			return theta *  v2 + (1-theta) * v1;
		}
		else 
		{
			float theta = quClamp<float>((x-x2)/(x3-x2),0,1);
			return theta *  v3 + (1-theta) * v2;
		}
	}
public:
	StrokeEffects()
	{
		inter1.setBaseValue(HALF_OPAQUE);
		inter1.setTargetValue(FULL_OPAQUE);
		inter2.setBaseValue(HALF_OPAQUE);
		inter2.setTargetValue(FULL_OPAQUE);

		mImg1 = QuStupidPointer<QuTintableGuiImage>(new QuTintableGuiImage());
		mImg1->loadImage(G_IMAGE_MANAGER.getFlyImage("images/sparkles-02.png"));

		mImg2 = QuStupidPointer<QuTintableGuiImage>(new QuTintableGuiImage());
		mImg2->loadImage(G_IMAGE_MANAGER.getFlyImage("images/sparkles-01.png"));

		mImg3 = QuStupidPointer<QuTintableGuiImage>(new QuTintableGuiImage());
		mImg3->loadImage(G_IMAGE_MANAGER.getFlyImage("images/sparks.png"));

		mImgUni = QuStupidPointer<QuTintableGuiImage>(new QuTintableGuiImage());
		mImgUni->loadImage(G_IMAGE_MANAGER.getFlyImage("images/unicorn.png"));
	}
	void draw()
	{
		fireworks.draw();
	}
	void update()
	{
		fireworks.update();
	}
	void stroked(int x, int y, float dv, float tv, float dt)
	{
		//stroke value dv
		//velocity dt
		//current sat tv
		
		//good stroke
		if(dv > 0)
		{
			//determine how many particles to make
			float mult = quClamp<float>(quAbs<float>(dv)/LOCAL_POS_SCORE_MOD_MAX,0,1);
			int quantity = 1+ mult * quRandf() * threeint(PARTICLE_COUNT_MIN,PARTICLE_COUNT_MID,PARTICLE_COUNT_MAX,CAT_LOSE,0,CAT_WIN,tv);
			//std::cout << "number parts " << quantity << std::endl;
			//determine what types of each particle we want to make
			float starp = CAT_STARS;
			float rainp = 0;
			float unip = 0;
			if(tv > CAT_RAINBOW_THRESH)
			{
				float theta = (tv-CAT_RAINBOW_THRESH)/(CAT_RAINBOW_MAX_THRESH-CAT_RAINBOW_THRESH);
				rainp = theta * CAT_RAINBOW_MAX + (1-theta) * CAT_RAINBOW_MIN;
			}
			if(tv > CAT_UNICORN_THRESH)
			{
				float theta = (tv-CAT_UNICORN_THRESH)/CAT_UNICORN_MAX_THRESH;
				unip = theta * CAT_UNICORN_MAX + (1-theta) * CAT_UNICORN_MIN;
			}
			float sump = starp + rainp + unip;

			for(int i = 0; i < quantity; i++)
			{
				QuStupidPointer<QuConstantForceParticle> cfp = QuStupidPointer<QuConstantForceParticle>(new QuConstantForceParticle(1));
				cfp->setPosition(x,y);
				cfp->mTimer.setTarget(quRandRange(5,5+3*floor(dt*30)));
				cfp->mColor.setBaseValue(inter1.interpolate(1/quMin<float>(1,tv)).d);
				cfp->s = 45;
				cfp->vs = 0;
				cfp->rot = quRandf() * 360;
				cfp->vr = quRandfSym()*100;
				cfp->ax = 0;
				cfp->ay = 3;
				float speed = 15;
				cfp->vy = -quRandf()*speed;
				cfp->vx = quRandfSym()*speed;

				float rn = quRandf();
				if(rn < starp/sump) //stars
				{
					cfp->s *= 0.5;
					fireworks.addParticle(mImg1,cfp);
				}
				else if(rn < (starp+rainp)/sump) //rainbows
				{
					cfp->s *= 0.8;
					fireworks.addParticle(mImg2,cfp);
					
				}
				else //unicorns
				{
					fireworks.addParticle(mImgUni,cfp);
				}
					
			}
		}
		//bad stroke
		else
		{
			//determine how many particles to make
			float mult = quClamp<float>(quAbs<float>(dv)/LOCAL_NEG_SCORE_MOD_MAX,0,1);
			int quantity = 1+ mult * quRandf() * threeint(PARTICLE_COUNT_MAX,PARTICLE_COUNT_MID,PARTICLE_COUNT_MIN,CAT_LOSE,0,CAT_WIN,tv);
			for(int i = 0; i < quantity; i++)
			{
				QuStupidPointer<QuConstantForceParticle> cfp = QuStupidPointer<QuConstantForceParticle>(new QuConstantForceParticle(1));
				cfp->setPosition(x,y);
				cfp->mTimer.setTarget(quMin(15.0f,100/(float)sqrt(-dv)));
				cfp->mColor.setBaseValue(inter2.interpolate(1/quMin<float>(1,-quMin(-0.1f,tv))).d);
				cfp->s = 20;
				cfp->vs = -3;
				cfp->rot = quRandf() * 360;
				cfp->vr = quRandfSym()*100;
				cfp->ax = 0;
				cfp->ay = 3;
				float speed = 20;
				quMax(15.0f,(float)sqrt(-dv)/2.0f);
				cfp->vy = -quRandfSym()*speed;
				cfp->vx = quRandfSym()*speed;
				fireworks.addParticle(mImg3,cfp);
			}
		}
		//std::cout << "stroke val " << dv << " current sat " << tv << " velocity " << dt << std::endl;

	}
};

class StrokeableObject
{
protected:
	CatApp * p;
public:
	StrokeableObject(CatApp * _p):p(_p){}
	virtual ~StrokeableObject(){}
	virtual void stroke(float sx, float sy, float ex, float ey, float t){}
	virtual QuStupidPointer<StrokeableObject> update() = 0;
	virtual void draw(){};
};

class ExitCat : public StrokeableObject
{
	QuStupidPointer<StrokeObject> mStroke;
	QuTimer mQuitTimer;
public:
	ExitCat(CatApp * _p);
	virtual QuStupidPointer<StrokeableObject> update()
	{
		mQuitTimer++;
		if(mQuitTimer.isExpired())
		{
			std::cout << "quit set..." << std::endl;
			G_GAME_GLOBAL.setQuit();
		}
		return QuStupidPointer<StrokeableObject>();
	}
	virtual void draw()
	{
		if(mStroke.isNull())
		{
			mStroke = QuStupidPointer<StrokeObject>(new StrokeObject());
			mStroke->init("images/losecat.png");
			mStroke->pump(9999999);
		}
		mStroke->draw();
	}
};

class WinCat : public StrokeableObject
{
	QuStupidPointer<StrokeObject> mStroke;
	QuTimer mQuitTimer;
public:
	WinCat(CatApp * _p);
	virtual QuStupidPointer<StrokeableObject> update()
	{
		mQuitTimer++;
		if(mQuitTimer.isExpired())
			G_GAME_GLOBAL.setQuit();
		return QuStupidPointer<StrokeableObject>();
	}
	virtual void draw()
	{
		if(mStroke.isNull())
		{
			mStroke = QuStupidPointer<StrokeObject>(new StrokeObject());
			mStroke->init("images/catmaster.png");
			mStroke->pump(9999999);
		}
		mStroke->draw();
	}
};

class StrokeableCredits : public StrokeableObject
{
	QuStupidPointer<StrokeableObject> mReturn;
	QuTimer mTimer;
	bool mStroked;
	QuImageDrawObject mImage;
	CatCamera mCam;
public:
	StrokeableCredits(CatApp * _p, QuStupidPointer<StrokeableObject> aReturn)
		:StrokeableObject(_p),mStroked(false),mTimer(0,30),mReturn(aReturn),
		mCam(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT)
	{
		mCam.setRotation(G_DR_90);
		mImage.setImage(G_IMAGE_MANAGER.getFlyImage("credits.png"));
	}
	virtual void stroke(float sx, float sy, float ex, float ey, float t)
	{
		mStroked = true;
	}
	virtual QuStupidPointer<StrokeableObject> update()
	{
		mTimer.update();
		if(mTimer.isExpired() && mStroked)
		{
			//help prevent licks
			QuStupidPointer<StrokeableObject> r = mReturn;
			mReturn.setNull();
			return r;
		}
		return QuStupidPointer<StrokeableObject>();
	}
	virtual void draw()
	{
		//TODO test
		glPushMatrix();
		mCam.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		mCam.setScene();
		glScalef(mCam.getRotatedWidth(),mCam.getRotatedHeight(),1);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		mImage.autoDraw();
		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glDisable(GL_BLEND);
		glPopMatrix();
	}
};



class StrokeableCat : public StrokeableObject
{
	QuStupidPointer<StrokeObject> mCat;
	QuStupidPointer<SoundProfile> mSoundProfile;
	StrokeEffects mFx;
	StrokeSound mSound;
	QuTriggerTimer mLast;

	QuAlgebraicTimedSignal<float> mSignal;
	float mScore;

	QuVector2<float> lastMovePos;
	QuAlgebraicTimedSignal<float> mDistance;
	//float mScore,mPrevScore;

	QuMultiTriggerTimer mLoseTimer;
public:
	StrokeableCat(CatApp * _p);
	~StrokeableCat()
	{
		mSoundProfile->unloadAllSounds();
	}
	virtual void stroke(float sx, float sy, float ex, float ey, float t);
	virtual QuStupidPointer<StrokeableObject> update();
	virtual void draw();
	void drawParticles()
	{
		mFx.draw();
	}
	void setCat(QuStupidPointer<StrokeObject> aCat)
	{
		mCat = aCat;
	}
	void reset()
	{
		mScore = 0;
		mSignal.reset();
		mLast.reset();
		mLoseTimer.setTargetAndReset(0);
	}
	float getScore()
	{
		return mScore;
		//return mSignal.weightedSum();
	}
};


class StrokeableNewMenu : public StrokeableObject
{
	QuStupidPointer<StrokeField> f1;
	QuStupidPointer<StrokeObject> mStroke;
	StrokeableCat mCat1;
	QuTimer mTimeSinceLastStroke;
	float v1;
public:
	StrokeableNewMenu(CatApp * _p):
	  StrokeableObject(_p),
	  mTimeSinceLastStroke(0,45),
	  mCat1(_p)
	{
		v1 = 0;
		long F = 0; long G = 0;
		float * data = StrokeObject::loadField("menu/funmenu.dat",F,G);
		f1 = QuStupidPointer<StrokeField>(new StrokeField(QuStupidPointer<float>(data,-1),F,G));
		mStroke = QuStupidPointer<StrokeObject>(new StrokeObject());
		mStroke->init("menu/funmenu.png");
		mStroke->pump(9999999);
		mCat1.setCat(mStroke);
	}
	virtual void stroke(float sx, float sy, float ex, float ey, float t);
	virtual QuStupidPointer<StrokeableObject> update();
	virtual void draw()
	{
		mCat1.draw();
	}
	void reset()
	{
		mCat1.reset();
	}
};

//DELETE ME
class StrokeableMenu : public StrokeableObject
{
	QuStupidPointer<StrokeField> f1,f2,f3;
	QuStupidPointer<StrokeObject> mStroke;
	StrokeableCat mCat1,mCat2,mCat3;
	QuTimer mTimeSinceLastStroke;
	float v1,v2,v3;
public:
	StrokeableMenu(CatApp * _p):
	  StrokeableObject(_p),
	  mTimeSinceLastStroke(0,45),
	  mCat1(_p),
	  mCat2(_p),
	  mCat3(_p)
	{
		v1 = v2 = v3 = 0;
		long F = 0; long G = 0;
		float * data = StrokeObject::loadField("menu/menu1.dat",F,G);
		f1 = QuStupidPointer<StrokeField>(new StrokeField(QuStupidPointer<float>(data,-1),F,G));
		data = StrokeObject::loadField("menu/menu2.dat",F,G);
		f2 = QuStupidPointer<StrokeField>(new StrokeField(QuStupidPointer<float>(data,-1),F,G));
		data = StrokeObject::loadField("menu/menu3.dat",F,G);
		f3 = QuStupidPointer<StrokeField>(new StrokeField(QuStupidPointer<float>(data,-1),F,G));
		mStroke = QuStupidPointer<StrokeObject>(new StrokeObject());
		mStroke->init("menu/menu.png");
		mStroke->pump(9999999);

		mCat1.setCat(mStroke);
		mCat2.setCat(mStroke);
		mCat3.setCat(mStroke);
	}
	virtual void stroke(float sx, float sy, float ex, float ey, float t);
	virtual QuStupidPointer<StrokeableObject> update();
	virtual void draw()
	{
		mCat1.draw();
		mCat2.drawParticles();
		mCat3.drawParticles();
	}
	void reset()
	{
		mCat1.reset(); mCat2.reset(); mCat3.reset();
	}
};


class CatApp
{
private:
	bool isMenu;
	int hasLoaded;
	QuFireworks fireworks;

	QuStupidPointer<StrokeableObject> man;
	StrokeLoader loader;
	SoundProfileLoader mSoundProfile;


	QuVector2<float> lastPos;
	uint64 timeSinceLastMouseMove;
	bool isCursorDown;
public:
	bool isDone;
	CatApp():hasLoaded(0),isMenu(true),isCursorDown(false)
	{
		QuStupidPointer<StrokeableCat> c(new StrokeableCat(this));
		c->setCat(getLoader().getNextCat());
		man = c.safeCast<StrokeableObject>();
		//man = QuStupidPointer<StrokeableObject>((StrokeableObject *)(new StrokeableNewMenu(this)));
	}
	~CatApp()
	{
	}
	void destroy()
	{
	}

	StrokeLoader & getLoader() { return loader; }
	SoundProfileLoader & getSoundLoader() { return mSoundProfile; }
	

	void drawDummyLoadingImage();
	void restartGame();

	void initialize();
	void setup();
	void update();
	void draw();

	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(QuScreenCoord crd );
	void mouseDragged(QuScreenCoord crd, int button);
	void mousePressed(QuScreenCoord crd, int button);
	void mouseReleased(QuScreenCoord crd, int button);

	//all hell break loose should this function be called
	//well actually, no, just make sure one resizes the QuCamera (and write some routines in QuCamera to do this)
	//TODO ^
	void windowResized(int w, int h){};

	void tiltUpdate();
};


class CatManager : public QuBaseManager
{
	bool mInit;
	QuStupidPointer<CatApp> g;
	//CIwResGroup * mSoundGroup;
public:
	CatManager():mInit(false)
	{
		
	}
	virtual ~CatManager()
	{
		//IwGetResManager()->DestroyGroup(mSoundGroup);
		g.setNull();
	}
	virtual void initialize()
	{
		g = QuStupidPointer<CatApp>(new CatApp());
		g->setup();
		//mSoundGroup = IwGetResManager()->LoadGroup("sounds/sound.group");
		mInit = true;
	}
	virtual void update()
	{
		g->update();
		g->draw();
		//QuAbsScreenIntBoxButton(QuBox2<int>(50,50,50,50)).drawRotatedRect(QuColor(1,0,0,1));
		//QuAbsScreenIntBoxButton(QuBox2<int>(50,50,50,50)).drawRotatedImage(G_IMAGE_MANAGER.getFlyImage("menu/menu.png"));
	}
	virtual void keyDown(int key){}
	virtual void keyUp(int key){ g->keyReleased(key); }
	virtual void singleDown(int button, QuScreenCoord crd)
	{ 
		//if(QuAbsScreenIntBoxButton(QuBox2<int>(50,50,50,50)).click(crd,G_DR_0))
		//	std::cout << "clicked" << std::endl;
		g->mousePressed(crd.reflect(G_SCREEN_REFLECT_VERTICAL),button);
	}
	virtual void singleUp(int button, QuScreenCoord crd){g->mouseReleased(crd.reflect(G_SCREEN_REFLECT_VERTICAL),button);}
	virtual void singleMotion(QuScreenCoord crd){g->mouseMoved(crd.reflect(G_SCREEN_REFLECT_VERTICAL));}
};
