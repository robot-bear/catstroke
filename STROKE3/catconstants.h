#pragma once
#include <string>

static const float MENU_HISS_THRESHOLD = -10;
static const float MENU_STROKE_TRIGGER_THRESHOLD = 40000;

static const float CAT_WIN = 15000;
static const float CAT_LOSE = -15000;



static const float CAT_STARS = 1; //determines how many stars to make

static const float CAT_RAINBOW_THRESH = 0.3f*CAT_WIN;
static const float CAT_RAINBOW_MAX_THRESH = 0.6f*CAT_WIN;
static const float CAT_RAINBOW_MIN = 0.0f;
static const float CAT_RAINBOW_MAX = 1; //determines proprotion of how many rainbows to make

static const float CAT_UNICORN_THRESH = 0.7f*CAT_WIN;
static const float CAT_UNICORN_MAX_THRESH = 1.0f*CAT_WIN;
static const float CAT_UNICORN_MIN = 0.2f;
static const float CAT_UNICORN_MAX = 1.0f;

static const float LOCAL_POS_SCORE_MOD_MAX = 75.0f; //sets mult to clamp(quabs(dv)/LOCALSCOREMODMAX,0,1) when stroke value > 0
static const float LOCAL_NEG_SCORE_MOD_MAX = 25.0f; // "  "   " ... < 0

static const float PARTICLE_COUNT_MAX = 8.0f; //will attempt to make rand()*5*mult particles each time stroke is called (i.e. each updated cycel)
static const float PARTICLE_COUNT_MID = 3.0f; //this is at score = 0, linearly interpolates this value, uses the reverse for sparks
static const float PARTICLE_COUNT_MIN = 1.0f; //this is at score in CAT_LOSE


static const float HULK_CAT_START = -0.2f;	//at this point, cat starts becomeing hulk cat
static const float HULK_CAT_MAX = -0.95f;
 
static const float NORMAL_CAT_COLOR[] = {1,1,1,1};
static const float HULK_CAT_COLOR[] = {0,1,0,1};


static const int IMPATIENT_CAT_TIME_FIRST = 50;
static const int IMPATIENT_CAT_TIME_SECOND_MIN = 10;
static const int IMPATIENT_CAT_TIME_SECOND_MAX = 30;

static const int CAT_MEMORY = 10000;
static const int NUMBER_HAPPY = 1;
static const int NUMBER_NEEDY = 1;
static const int NUMBER_UNHAPPY = 1;