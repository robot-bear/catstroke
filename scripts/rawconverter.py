import sys
import os
from subprocess import call

def convertAllWavesInDirectory(dir):
    files = os.listdir(dir)
    for e in files:
        fn = os.path.splitext(e)
        if(fn[1] == '.wav'):
            #-V verbose, -r rate 44100, -w?, -c channels 1, -s enconding type signed integer
            print("sox -V -r 44100  -c 1 -s " + os.path.abspath(os.path.join(dir,fn[0] + ".wav ")) + os.path.abspath(os.path.join(dir,fn[0] + ".raw ")))
            call("sox -V -r 44100  -c 1 -s " + os.path.abspath(os.path.join(dir,fn[0] + ".wav ")) + os.path.abspath(os.path.join(dir,fn[0] + ".raw ")),shell=True)



def main(argv):
    convertAllWavesInDirectory(argv[0])




if __name__ == "__main__":
    main(sys.argv[1:])
