import sys
import os
from os.path import abspath
from os.path import join
from subprocess import Popen


def convertAllWavesInDirectory(dir):
    files = os.listdir(dir)
    for e in files:
        fn = os.path.splitext(e)
        if(fn[1] == '.wav'):
            #-V verbose, -r rate 44100, -w?, -c channels 1, -s enconding type signed integer
            #Popen("sox -V -r 44100 -w -c 1 -s " + abspath(join(dir,fn[0] + ".raw ")) + abspath(join(dir,e)),shell=True)
            Popen("madplay " + abspath(join(dir,fn[0] + ".wav")) + " -b 16 -R 44100 -m -o " + abspath(join(dir,fn[0] + ".raw")),shell=True)



def main(argv):
    convertAllWavesInDirectory(argv[0])




if __name__ == "__main__":
    main(sys.argv[1:])
